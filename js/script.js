/**
 * @file
 * Opens the print window.
 */

document.addEventListener('DOMContentLoaded', function print() {
  window.print();
});
