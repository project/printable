/**
 * @file
 * Closes the print window.
 */

document.addEventListener('DOMContentLoaded', function closePrintWindow() {
  window.print();
  window.close();
});
